﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml.Linq;
using System.Xml;
using FirebirdSql.Data.FirebirdClient;

namespace ConvertOrder
{
    class Program
    {
        static int Main(string[] args)
        {
//            string shablon =
//                @"Клиент       : [CodeClient]
//Получатель   : [CodeDostavki]
//Оплата       : 0
//ID заказа    : [ZakazCode]
//Клиентский ID: [ZakazCode]
//Дата заказа  : [DataOrder] 
//Позиций      : [Rows]
//Версия EXE   : ZCONVERT
//Версия CFG   : ZCONVERT
//Статус CFG   : ZCONVERT
//Прайс-лист   : [DataOrder] 
//Комментарий  : [Comment]
//";
           
            try
            {
                Encoding encoding = Encoding.GetEncoding(1251);
                #region Проверка параметров
                if (args.Length < 2)
                {
                    System.Console.WriteLine("Convert <файл заявки> <файл шаблона заявки>");
                    return 1;
                }

                if (!System.IO.File.Exists(args[0]) || !System.IO.File.Exists(args[1]))
                {
                    System.Console.WriteLine(DateTime.Now.ToString("dd:MM:yyyy hh:mm:ss") + " Convert <файл заявки> <файл шаблона заявки>");
                    return 1;
                }       
                #endregion
                System.Console.WriteLine(DateTime.Now.ToString("dd:MM:yyyy hh:mm:ss") + " Начинаем конвертацию. " + " " + args[0]);
                //Читаем файл заявки
                List<Row> rows = new List<Row>();
                Order order = new Order();
                XmlDocument doc = new XmlDocument();
                doc.Load(args[0]);
                IEnumerable<string> LinesCod = File.ReadAllLines(args[1], encoding);
                XmlNode orderxml = doc.SelectSingleNode("Файл/ЗапросНаРезерварование");
                if (orderxml.Attributes["Номер"].Value != null)
                {
                    order.CodeZakaz = orderxml.Attributes["Номер"].Value;
                }
                  Cod cod = new Cod{ CodeClient = "" , CodeDostavki = ""};
                  if (orderxml.Attributes["ИдентификаторТорговойТочки"].Value != null)
                  {
                      cod = GetCodeFromASU(orderxml.Attributes["ИдентификаторТорговойТочки"].Value.Trim(), args[0].Split('_')[1].Substring(0,10));
                  }
                  if (orderxml.Attributes["Дата"].Value != null)
                  {
                      order.DataOrder =  orderxml.Attributes["Дата"].Value.Substring(6, 2) + "."
                                       + orderxml.Attributes["Дата"].Value.Substring(4, 2) + "."
                                       + orderxml.Attributes["Дата"].Value.Substring(0, 4);
                  }
                  if (orderxml.Attributes["АдресТорговойТочки"].Value != null)
                  {
                      order.Comment = orderxml.Attributes["АдресТорговойТочки"].Value;
                  }
                order.CodeDostavki = cod.CodeDostavki;
                order.CodeClient = cod.CodeClient;
                order.TimeOrder = "";
                int i = 0;
                foreach (XmlNode item in doc.SelectNodes("//Номенклатура"))
                    {
                        rows.Add(new Row() { Code = item.Attributes["Идентификатор"].Value, Count = item.Attributes["Количество"].Value });     
                        i++;
                     }
                    order.rows = rows;
                    order.Rows = rows.Count.ToString();
                   
                    //формируем заявку СИА
                    IEnumerable<string> shablon = File.ReadLines(args[1], encoding);
                    StringBuilder ordersia = new StringBuilder();
                    ordersia.Append(string.Join<string>("\r\n", shablon).Replace("[CodeZakaz]", order.CodeZakaz)
                                                                        .Replace("[CodeClient]", order.CodeClient)
                                                                        .Replace("[CodeDostavki]", order.CodeDostavki)
                                                                        .Replace("[AddressDostavki]", order.AddressDostavki)
                                                                        .Replace("[DataOrder]", order.DataOrder)
                                                                        .Replace("[Rows]", order.rows.Count.ToString())
                                                                        .Replace("[Code]", order.rows[0].Code)
                                                                        .Replace("[Count]", order.rows[0].Count)
                                                                        .Replace("[Comment]",order.Comment)).AppendLine();
                    foreach (var item in order.rows)
                    {
                        ordersia.Append(new string(' ', 50 - item.Code.Length)).Append(item.Code).Append(new string(' ', 10 - item.Count.Length)).Append(item.Count).AppendLine();
                    }
                    //пишем в файл 
                    File.WriteAllText(args[0] + ".txt", ordersia.ToString(), encoding);

            }
            catch (Exception e)
            {
                System.Console.WriteLine(DateTime.Now.ToString("dd:MM:yyyy hh:mm:ss") + e.Message);
                return 1;
            }

            return 0;
        }

        private static Cod GetCodeFromASU(string addresscode, string inn)
        {
            Cod cod = new Cod();
            using (FbConnection RefConnection = new FbConnection("User ID=TWUSER;Password=54321; Database=./db/Siafil.gdb; DataSource=asusrv; Charset=NONE; providerName=\"System.Data.SqlClient\""))
            {
                try
                {
                    RefConnection.Open();
                    FbCommand readCommand = new FbCommand(String.Format("select n.nid, n.nidlib from adrdost as a join nab as n on a.adrdostid=n.nid, nab as n1  where n.nidlib = n1.nidlib and n1.nid1=4310 and n1.nconcept=104001 and substring(n1.ninfo from 1 for 10)= '{0}'  and a.adrdostcode ='{1}'", 
                        inn,addresscode), RefConnection); 
                    FbDataReader myreader = readCommand.ExecuteReader();
                    int i = 0;
                    while (myreader.Read())
                    {
                        cod.CodeDostavki = myreader.GetString(0);
                        cod.CodeClient = myreader.GetString(1);
                        i++;
                    }
                    RefConnection.Close();
                    if (i > 1) throw new Exception("Не уникальный код");
                }
                catch (Exception e)
                {
                    throw new Exception(e.Message);
                }
            }
            return cod;
        }
        public class Order
        {
            public string CodeClient { get; set; }
            public string CodeDostavki { get; set; }
            public string AddressDostavki { get; set; }
            public string Rows { get; set; }
            public string DataOrder { get; set; }
            public string TimeOrder { get; set; }
            public string DataPrice { get; set; }
            public string TimePrice { get; set; }
            public string Comment { get; set; }
            public List<Row> rows { get; set; }
            public string CodeZakaz { get; set; }
        }
        public class Row
        {
            public string Code { get; set; }
            public string Count { get; set; }
            public string CodeDostavkiFarm { get; set; }
        }
        public class Cod
        {
            public string CodeDostavki { get; set; }
            public string CodeClient { get; set; }
        }

    }
}
